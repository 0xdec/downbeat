const state = {
  isDrawerMini: true,
  isDarkTheme: false,
  audio: ''
}

const mutations = {
  DRAWER_NORMAL (state) {
    state.isDrawerMini = false
  },
  DRAWER_MINI (state) {
    state.isDrawerMini = true
  },
  THEME_LIGHT (state) {
    state.isDarkTheme = false
  },
  THEME_DARK (state) {
    state.isDarkTheme = true
  },
  SET_AUDIO (state, audio) {
    state.audio = audio
  }
}

const actions = {
  setDrawerNormal ({ commit }) {
    commit('DRAWER_NORMAL')
  },
  setDrawerMini ({ commit }) {
    commit('DRAWER_MINI')
  },
  setLightTheme ({ commit }) {
    commit('THEME_LIGHT')
  },
  setDarkTheme ({ commit }) {
    commit('THEME_DARK')
  },
  setAudio ({ commit }, audio) {
    commit('SET_AUDIO', audio)
  }
}

const modules = {
  drill: {
    namespaced: true,
    state: {
      accuracy: []
    },
    mutations: {
      SET_ACCURACY (state, accuracy) {
        state.accuracy = accuracy
      }
    },
    actions: {
      setAccuracy ({ commit }, accuracy) {
        commit('SET_ACCURACY', accuracy)
      }
    }
  },
  music: {
    namespaced: true,
    state: {},
    mutations: {},
    actions: {}
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  modules
}
