const state = {
  performers: [],
  seasons: []
}

const mutations = {
  DATABASE_CHANGED (state, data) {
    state.performers = data.performers
    state.seasons = data.seasons
  }
}

const actions = {
  changeDatabase ({ commit }, data) {
    commit('DATABASE_CHANGED', data)
  }
}

export default {
  state,
  mutations,
  actions
}
