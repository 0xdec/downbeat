const state = {
  season: '',
  show: '',
  act: ''
}

const mutations = {
  DIRECTORY_CHANGED (state, dir) {
    state.season = dir.season
    state.show = dir.show
    state.act = dir.act
  }
}

const actions = {
  changeDirectory ({ commit }, dir) {
    commit('DIRECTORY_CHANGED', dir)
  }
}

export default {
  state,
  mutations,
  actions
}
