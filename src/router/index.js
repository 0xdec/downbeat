import Vue from 'vue'
import Router from 'vue-router'
import Meta from 'vue-meta'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Router)
Vue.use(Meta)
Vue.use(Vuetify, {
  theme: {
    primary: '#13294B',
    secondary: '#424242',
    accent: '#E84A27'
  }
})

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('@/views/Home.vue')
    },
    {
      path: '/drill',
      name: 'drill',
      component: () => import('@/views/Drill.vue')
    },
    {
      path: '/music',
      name: 'music',
      component: () => import('@/views/Music.vue')
    },
    {
      path: '/tools',
      name: 'tools',
      component: () => import('@/views/Tools.vue')
    },
    {
      path: '/settings',
      name: 'settings',
      component: () => import('@/views/Settings.vue')
    }
  ]
})
