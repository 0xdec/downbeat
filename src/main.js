import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import { sync } from 'vuex-router-sync'
import axios from 'axios'
import './registerServiceWorker'

sync(store, router)
Vue.config.productionTip = false
Vue.http = Vue.prototype.$http = axios

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
